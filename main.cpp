#include "Ullmann.h"

// bin input_dir output_dir
int main(int argc, char **argv)
{
    std::string input_dir, output_dir;
    if (argc > 1)
        input_dir = argv[1];
    if (argc > 2)
        output_dir = argv[2];
    std::string f;
    std::vector<std::string> qfs;
    for (const auto &entry : std::filesystem::directory_iterator(input_dir))
    {

        std::string name = entry.path().string();
        if (name.find(DATA_GRAPH_FILE_SUFFIX) != std::string::npos)
            f = name;
        if (name.find(QURTY_GRAPH_FILE_SUFFIX) != std::string::npos)
            qfs.push_back(name);
    }
    std::vector<long long> qset_times(qfs.size(), 0);
    std::vector<long long> qset_count(qfs.size(), 0);
    std::vector<int> qfnos;
    for (auto &qf : qfs)
    {
        size_t i1 = qf.find_last_of('/');
        size_t i2 = qf.find_last_of('.');
        // xxx/Q16.my
        int qfno = std::stoi(qf.substr(i1 + 2, i2 - i1 - 2));
        qfnos.push_back(qfno);
    }

    int qfno;
    int gno, qno;
    char nouse;
    std::ifstream fin(f);
    fin >> nouse;
    while (1)
    {
        fin >> nouse >> gno;
        std::cout << gno << std::endl;
        if (gno < 0)
            break;
        Graph g;
        g.read_graph(fin);
        for (size_t i = 0; i < qfs.size(); ++i)
        {
            std::string qf = qfs[i];
            qfno = qfnos[i];
            std::ifstream qfin(qf);
            qfin >> nouse;
            while (1)
            {
                qfin >> nouse >> qno;
                std::cout << gno << " " << qfno << " " << qno << std::endl;
                if (qno < 0)
                    break;
                Graph q;
                q.read_graph(qfin);
                auto result_f = output_dir + "/" + std::to_string(gno) + "_" + std::to_string(qfno) + "_" + std::to_string(qno) + OUTPUT_FILE_SUFFIX;
                Ullmann ins(g, q, result_f);
                auto t0 = std::chrono::steady_clock::now();
                ins.run();
                auto t1 = std::chrono::steady_clock::now();
                qset_times[i] += (std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0)).count();
                ++qset_count[i];
            }
        }
    }

    std::cout << "Qset_number\tave_query_time\ttotal_query_time\t#quries" << std::endl;
    for (size_t i = 0; i < qfs.size(); ++i)
        std::cout << qfnos[i] << "\t" << ((long double)qset_times[i]) / qset_count[i] << "\t" << qset_times[i] << "\t" << qset_count[i] << std::endl;
    return 0;
}