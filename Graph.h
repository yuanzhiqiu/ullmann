#ifndef _GTAPH_H
#define _GTAPH_H

#include "config.h"

// adjancent matrix
class Graph
{
public:
    VertexType N;
    std::vector<EdgeLabelType> mat; // N*N
    std::vector<VertexLabelType> vertex_label;
    std::vector<std::unordered_map<EdgeLabelType, EdgeType>> nbr_label_freq;

    void read_graph(std::ifstream &fin)
    {
        // read N
        std::streampos pos = fin.tellg();
        std::string line;
        N = 0;
        getline(fin, line); // \r\n
        while (getline(fin, line))
        {
            if (line[0] != 'v')
                break;
            ++N;
        }
        fin.seekg(pos);

        // read graph
        vertex_label.resize(N, -1);
        nbr_label_freq.resize(N);
        mat.resize(N * N, -1);
        char id;
        VertexType u, v, lb;
        while (fin >> id)
        {
            if (id == 'e')
                break;
            fin >> u >> lb;
            // std::cout << id << " " << u << " " << lb << std::endl;
            vertex_label[u] = lb;
        }
        std::unordered_map<EdgeLabelType, EdgeType>::iterator it;
        do
        {
            if (id != 'e')
                break;
            fin >> u >> v >> lb;
            // std::cout << id << " " << u << " " << v << " " << lb << std::endl;
            mat[u * N + v] = lb;
            mat[v * N + u] = lb;
            if ((it = nbr_label_freq[u].find(lb)) != nbr_label_freq[u].end())
                it->second++;
            else
                nbr_label_freq[u][lb] = 1;
            if ((it = nbr_label_freq[v].find(lb)) != nbr_label_freq[v].end())
                it->second++;
            else
                nbr_label_freq[v][lb] = 1;
        } while (fin >> id);
    }
    // return v can cover u
    bool nbr_label_freq_cover(VertexType u, std::unordered_map<EdgeLabelType, EdgeType> &vcnt) const
    {
        std::unordered_map<EdgeLabelType, EdgeType>::iterator it;
        for (auto &pr : nbr_label_freq[u])
        {
            EdgeLabelType lb = pr.first;
            if ((it = vcnt.find(lb)) != vcnt.end())
            {
                if (it->second < pr.second)
                    return false;
            }
            else
                return false;
        }
        return true;
    }
};

#endif // _GTAPH_H