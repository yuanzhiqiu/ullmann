#include <iostream>
#include <fstream>
#include <filesystem>
#include <chrono>
#include <vector>
#include <unordered_map>

typedef int VertexType;
typedef int VertexLabelType;
typedef int EdgeType;
typedef int EdgeLabelType;

#define SILENCE_RESULT_OUTPUT

#define DATA_GRAPH_FILE_SUFFIX ".data"
#define QURTY_GRAPH_FILE_SUFFIX ".my"
#define OUTPUT_FILE_SUFFIX ".txt"
